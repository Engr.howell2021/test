import React, { useState, useEffect} from 'react';
import Swal from 'sweetalert2';
import {useNavigate} from 'react-router-dom';
import {Form,Button} from 'react-bootstrap';


export default function Register(){
	const navigate = useNavigate();

	// hooks
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [gender, setGender] = useState ('');
	const [mobileNum, setMobileNum] = useState ('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	// Conditional Rendering
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
	// Validation to enable submit button when all fields are populated and both password match
	if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && gender !== '' && mobileNum !== '' ) && (password1 === password2)) {
		
		setIsActive(true)
	} 

	else {
		setIsActive(false)
	}

	}, [email, password1, password2, firstName, lastName, gender, mobileNum])

	function clear(){
		setFirstName('');
		setLastName('');
		setEmail('');
		setGender('');
		setMobileNum('');
		setPassword1('');
		setPassword2('');
	}

	function registerUser(e){
		e.preventDefault()
		fetch('https://sheltered-ridge-64247.herokuapp.com/users/register', {
			method:'POST',
			headers: {'Content-Type':'application/json'},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				gender: gender,
				mobileNum: mobileNum,
				password: password1
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result);
			if(result){
				
				Swal.fire({
					title:"Registered",
					icon: "success",
					text: `${email} Successfully Registered`
				})
				navigate('/login')
			} else {
				Swal.fire({
					title:"Error",
					icon: "error",
					text: "Something Went Wrong, Please try again"
				})
			}
			clear();
		})
	}

	return(
		<Form className="mt-3" class="form_wrapper" onSubmit={(e) => registerUser(e)}>
			<h3>Sign Up!</h3>
			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter First Name"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter Last Name"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Gender</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter gender"
					required
					value={gender}
					onChange={e => setGender(e.target.value)}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter mobile number"
					required
					value={mobileNum}
					onChange={e => setMobileNum(e.target.value)}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)} 
				/>
			</Form.Group>
			{ isActive ?
				<Button className="mt-2" variant="primary" type="submit">Register</Button>
				:
				<Button className="mt-2" variant="primary" type="submit" disabled>Register</Button>
			}
		</Form>
	)
}