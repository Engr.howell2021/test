import React from "react";
import {Row, Col, Card, Button} from 'react-bootstrap'

export default function Highlights(){
    return (
        <Row >
            <Col xs={12} md={4}>
                <Card className = "cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                        Cat Lovers Shirts
                        </Card.Title>
                        <Card.Text>
                        <Button>Details</Button>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        
            <Col xs={12} md={4}>
                <Card className = "cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                        Dog Lovers Shirts
                        </Card.Title>
                        <Card.Text>
                        <Button>Details</Button>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            
            <Col xs={12} md={4}>
                <Card className = "cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                        <h1>Anime</h1>
                        </Card.Title>
                        <Card.Text>
                            <Button>Details</Button>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
        
    )
}
