import React, {useContext} from 'react';
import UserContext from '../UserContext';

import { Link } from 'react-router-dom';
import {Navbar, Nav} from 'react-bootstrap';


export default function AppNavbar(){

	const {user} = useContext(UserContext);

	return (
			<Navbar expand="lg" sticky="top">
                <img src={require('./images/GraphixTee-HiRes.png')} alt='' height={30} width={30} className='d-inline-block align-top ms-4'/>
                <Navbar.Brand className="ms-2" href="#">Graphix Tee</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>

				<Navbar.Collapse  id="basic-navbar-nav">
					
					<Nav className="ms-auto">
						<Nav.Link as={ Link } to='/'>Home</Nav.Link>
						<Nav.Link as={ Link } to='/product'>Tee-Shirts</Nav.Link>
						<Nav.Link as={ Link } to='/contact'>Contact Us</Nav.Link>

						{ (user.accessToken !== null) ?
							<>
								<Nav.Link as={ Link } to='/logout'>Logout</Nav.Link>
								
							</>
							:
							<>

								<Nav.Link as={ Link } to='/register'>Register</Nav.Link>
								<Nav.Link as={ Link } to='/login'>Login</Nav.Link>
							</>
						}

					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)
}
