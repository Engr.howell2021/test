import React from 'react';
import { Row, Col, Button} from 'react-bootstrap'


export default function Banner() {

    return(
        <Row>
            <Col className= "p-5">
                <h1 className="mb-3">Graphix Tee</h1>
                
                <p className="my-2">Newly built tshirt store with unique designs.</p>
                <Button variant="primary">Grab Now!</Button>
            </Col>
        </Row>
    )
}